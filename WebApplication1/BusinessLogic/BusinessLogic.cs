﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace WebApplication1.Models
{
    public class BusinessLogic
    {
        public List<myList> SplitedFile()
        {
            string[] mas = null;
            List<myList> list = new List<myList>();
            string pattern1 = @"\n";
            string pattern2 = @"\t";
            mas = System.Text.RegularExpressions.Regex.Split(GetFile(), pattern1);
            foreach (string result in mas)
            {
                if (result == "")
                    break;
                myList mylist = new myList();
                string[] ms = null;
                ms = System.Text.RegularExpressions.Regex.Split(result, pattern2);
                mylist.id = Convert.ToInt32(ms[0]);
                mylist.date = ms[1];
                mylist.ip = ms[2];
                mylist.log = ms[3];
                mylist.vote = Convert.ToInt32(ms[4]);
                list.Add(mylist);
            }

            return list;
        }

        public string GetFile()
        {
            WebClient file = new WebClient();
            string text = file.DownloadString("https://mmotop.ru/votes/3d1c5d44db6ab17f74f1b90c83d88ec7fa0ab273.txt?603819ac288008a327adb6e088b6c663");
            return text;
        }
    }
}