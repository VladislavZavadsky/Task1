﻿using System.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
using WebApplication1.DataAcces;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AuthForm()
        {
            return PartialView();
        }

        [HttpPost]
        public string Authorization(string Login, string Password)
        {
            if (ModelState.IsValid)
            {
               
                BusinessLogic el = new BusinessLogic();
                DataAccess da = new DataAccess();
                List<myList> list = el.SplitedFile();
                return da.Load(Login,Password);
            }
            return "Validation Error.";
        }

        [HttpPost]
        public string Registration(string Login, string Password, string PasswordConfirm)
        {
            if (ModelState.IsValid)
            {
                DataAccess da = new DataAccess();
                string daResp = da.Load(Login, Password);
                return "Account Created. " + daResp;
            }
            return "Validation Error";
        }
    }
}