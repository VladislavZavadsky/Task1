﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace WebApplication1.DataAcces
{
    public class DataAccess
    {
        public string Load(string login, string password)
        {
            string connectionString = WebConfigurationManager.ConnectionStrings["vlad.z_Task1"].ConnectionString;

            SqlConnection connection = new SqlConnection(connectionString);

            SqlCommand command = new SqlCommand("AddAccount",connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;

            command.Parameters.Add("@login", SqlDbType.NChar, 16).Value = login;
            command.Parameters.Add("@password", SqlDbType.NChar, 32).Value = password;
            command.Parameters.Add("@gold", SqlDbType.Int).Value = 0;

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                return "SqlExeption: " + e;
            }

            connection.Close();

            return "DataSuccessConection";
        }
    }
}