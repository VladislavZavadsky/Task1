﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class RegistrModel
    {
        [Required(ErrorMessage="Поле должно быть заполнено")]
        [DataType(DataType.Text)]
        [StringLength(16,MinimumLength=5,ErrorMessage="Логин должен быть не менее 5 и не более 16 символов")]
        public string Login { get; set; }
        
        [Required(ErrorMessage = "Поле должно быть заполнено")]
        [DataType(DataType.Password)]
        [StringLength(16, MinimumLength = 5, ErrorMessage = "Логин должен быть не менее 5 и не более 16 символов")]
        public string Password { get; set; }

        [Compare("Password",ErrorMessage="Пароли не совпадают")]
        [Required(ErrorMessage = "Поддтвердите свой пароль")]
        [DataType(DataType.Password)]
        [StringLength(16, MinimumLength = 5)]
        public string PasswordConfirm { get; set; }
    }
}